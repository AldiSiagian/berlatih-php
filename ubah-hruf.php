<?php
function ubah_huruf($string){
    $alphabet = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

    for($i = 0; $i < strlen($string); $i++){
        for($j = 1; $j < count($alphabet); $j++){
            if($string[$i] == $alphabet[count($alphabet)-1]){
                $string[$i] = $alphabet[0];
                break;
            }else if($string[$i] == $alphabet[$j-1]){
                $string[$i] = $alphabet[$j];
                break;
            }
        }
    }

    return $string;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangatzzz'); // tfnbohbu

?>
